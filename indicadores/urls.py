from django.urls import path
from indicadores import views

urlpatterns = [

    # EMPLEADO

    ###  evaluaciones a si mismo
    path('evaluaciones_empleado',views.evaluaciones_empleado, name="evaluaciones_empleado"),
    path('empleado_evaluacion/<int:id>/ver/',views.ver_evaluacion_empleado, name="ver_evaluacion_empleado"),
    path('evaluacion/empezar/',views.nueva_evaluacion, name="nueva_evaluacion"),
    path('empleado_evaluacion_competencia/<int:id>/continuar/',views.evaluacion_competencia, name="evaluacion_competencia"),

    # ADMIN

    ###  evaluaciones general
    path('evaluaciones_admin_general/',views.evaluaciones_admin_general, name="evaluaciones_admin_general"),
    path('evaluacion_admin_general/<int:id>/ver/',views.ver_evaluacion_admin_general, name="ver_evaluacion_admin_general"),
    
    ###  evaluaciones anuales
    path('evaluaciones_admin_anual/',views.evaluaciones_admin_anual, name="evaluaciones_admin_anual"),
    
    ###  evaluaciones a los empleados - equipo de trabajo
    path('empleados_evaluaciones_admin/',views.evaluaciones_admin, name="evaluaciones_admin"),
    path('empleados_evaluacion_admin/<int:id>/ver/',views.ver_evaluacion_admin_empleado, name="ver_evaluacion_admin_empleado"),
    path('evaluacion_admin/<int:id>/',views.responder_evaluacion_empleado, name="evaluacion_admin"),
    path('evaluacion_competencia_admin/<int:id>/continuar/',views.evaluacion_competencia_admin, name="evaluacion_competencia_admin"),

    ###  evaluaciones a si mismo
    path('admin_evaluaciones/',views.admin_evaluaciones, name="admin_evaluaciones"),
    path('admin_evaluacion/<int:id>/ver/',views.ver_evaluacion_admin, name="ver_evaluacion_admin"),
    path('admin_nueva_evaluacion/empezar/',views.admin_nueva_evaluacion, name="admin_nueva_evaluacion"), # nueva evaluacion
    path('admin_evaluacion_competencia/<int:id>/terminar/',views.admin_evaluacion_competencia, name="admin_evaluacion_competencia"),

    # LIDER

    ###  evaluaciones a los empleados
    path('empleados_evaluaciones_lider/',views.evaluaciones_lider, name="evaluaciones_lider"),
    path('empleados_evaluacion_lider/<int:id>/ver/',views.ver_evaluacion_lider_empleado, name="ver_evaluacion_lider_empleado"),
    path('evaluacion_lider/<int:id>/',views.responder_evaluacion, name="evaluacion_lider"),
    path('evaluacion_competencia_lider/<int:id>/continuar/',views.evaluacion_competencia_lider, name="evaluacion_competencia_lider"),

    ###  evaluaciones a si mismo
    path('lider_evaluaciones/',views.lider_evaluaciones, name="lider_evaluaciones"),
    path('lider_evaluacion/<int:id>/ver/',views.ver_evaluacion_lider, name="ver_evaluacion_lider"),
    path('lider_nueva_evaluacion/empezar/',views.lider_nueva_evaluacion, name="lider_nueva_evaluacion"), # nueva evaluacion
    path('lider_evaluacion_competencia/<int:id>/terminar/',views.lider_evaluacion_competencia, name="lider_evaluacion_competencia"),

    # DIRECTOR

    ###  evaluaciones a los lideres
    path('director_evaluaciones/',views.director_evaluaciones, name="director_evaluaciones"),
    path('director_evaluacion/<int:id>/ver/',views.ver_evaluacion_director_lider, name="ver_evaluacion_director_lider"),
    path('evaluacion_objetivo_director/<int:id>/',views.evaluacion_objetivo_director, name="evaluacion_objetivo_director"),
    path('evaluacion_competencia_director/<int:id>/continuar/',views.evaluacion_competencia_director, name="evaluacion_competencia_director"),

    ###  evaluaciones a si mismo
    path('evaluaciones_director/',views.evaluaciones_director, name="evaluaciones_director"),
    path('evaluacion_director/<int:id>/ver/',views.ver_evaluacion_director, name="ver_evaluacion_director"),
    path('director_nueva_evaluacion/empezar/',views.director_nueva_evaluacion, name="director_nueva_evaluacion"), # nueva evaluacion
    path('director_evaluacion_competencia/<int:id>/terminar/',views.director_evaluacion_competencia, name="director_evaluacion_competencia"),

 ]