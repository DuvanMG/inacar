from django.shortcuts import render, redirect
from indicadores.models import Evaluacion, RespuestaObjetivo, \
    Pregunta, RespuestaCompetencia, InformeAnual, Competencia
from indicadores.forms import RespuestaObjetivoEvaluadoForm, RespuestaObjetivoEvaluadorForm, \
    RespuestaCompetenciaEvaluadoForm, RespuestaCompetenciaEvaluadorForm
from datetime import date
from django.http import HttpResponse
from django.db.models import Avg
today  = date.today()

############################# EVALUACIONES DE LOS EMPLEADOS

def ver_evaluacion_empleado(request, id):

    promedio_total = 0
    competencias = []
    subcompetencias = []

    # Obtener la evaluación y sus respuestas
    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    # Sacar el promedio de cada competencia
    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado] 
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    # Calcular las competencias del evaluador y el evaluado con sus respectivos porcentajes
    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    # Calcular los objetivos del evaluador y el evaluado con sus respectivos porcentajes
    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    # Calcular las competencias y los objetivos con sus respectivos porcentajes
    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    # Guardar los porcentajes y el total de la evaluación
    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    # Obtener las competencias y subcompetencias
    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/empleado/ver_evaluacion.html', greeting)


def evaluaciones_empleado(request):

    empleado = request.user.empleado
    evaluaciones = Evaluacion.objects.filter(empleado=empleado)
    anio_actual = today.year 

    try:
        evaluaciones_anuales  = Evaluacion.objects.filter(empleado=empleado, fecha__year=anio_actual)
        non_none_resultados = [evaluacion.resultado_final for evaluacion in evaluaciones_anuales if evaluacion.resultado_final is not None]

        if non_none_resultados:
            # Calcule el resultado anual sólo si hay evaluaciones disponibles para el año.
            promedio_anual = sum(non_none_resultados) / len(evaluaciones_anuales)
        else:
            promedio_anual = 0  # Establece un valor por defecto '0' maneja el escenario apropiadamente.

        # Guarda el 'promedio_anual' calculado en el atributo 'resultado'.
        informe_anual = InformeAnual.objects.get(empleado=empleado, periodo__year=anio_actual)
        informe_anual.resultado = promedio_anual
        informe_anual.save()

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['promedio_anual'] = promedio_anual  
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    except InformeAnual.DoesNotExist:

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    template_name = 'indicadores/empleado/empleado_evaluaciones.html'
    return render(request, template_name, greeting)

# Realizar evaluación (empleado)
def nueva_evaluacion(request):

    # si es operativo que no muestre objetivos solo competencias
    empleado = request.user.empleado
    objetivos = empleado.cargo.objetivos.all() 

    if request.method == "GET": # crear una nueva instancia al empezar
        evaluacion = Evaluacion.objects.create(empleado=empleado, lider=empleado.lider)

    if objetivos:     ## si el empleado tiene objetivos asociados

        ## crear nueva evaluacion con formulario de objetivos
        ### formulario de objetivos para el cargo del empleado
        forms_objetivo = [] 

        for objetivo in objetivos:
            form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST or None, prefix=f'objetivo_{objetivo.pk}')
            forms_objetivo.append((objetivo, form_objetivo))
            
        if 'objetivo' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id')
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)

            for objetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST, prefix=f'objetivo_{objetivo.pk}')
                if form_objetivo.is_valid():
                    respuesta = form_objetivo.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.objetivo = objetivo # asociarloo al objetivo
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.
 
            return redirect('evaluacion_competencia', id=evaluacion.id)
        
        greeting = {
            'empleado': empleado,
            'forms_objetivo':forms_objetivo,      
            'evaluacion':evaluacion    
        }
    
    else:         
        ## si el empleado NO TIENE objetivos asociados       
        
        ######## formularios de comptencias get
        preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=empleado.nivel_administrativo)
        
        forms_competencia = []
        
        for pregunta in preguntas:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
            forms_competencia.append((pregunta, form_competencia))
        
        if 'competencia' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id', None)
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)

            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.pregunta = pregunta # asociarloo a la pregunta
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.

            try:
                informe_anual = InformeAnual.objects.get(empleado=empleado, lider=empleado.lider, periodo__year=today.year)

            except InformeAnual.DoesNotExist:
                    informe_anual =  InformeAnual.objects.create(empleado=empleado, lider=empleado.lider, periodo=evaluacion.fecha)
                    evaluacion.informe_anual=informe_anual
                    evaluacion.save()
                    informe_anual.save()  

            return redirect('evaluaciones_empleado')
            
        greeting = {
                'empleado': empleado,
                'forms_competencia':forms_competencia, 
                'evaluacion':evaluacion    
            }

    template_name = 'indicadores/empleado/evaluacion_empleado.html'
    return render(request, template_name, greeting)

def evaluacion_competencia(request, id):
    
    empleado = request.user.empleado
    evaluacion = Evaluacion.objects.get(id=id)
    preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=empleado.nivel_administrativo)

    forms_competencia = []
    for pregunta in preguntas:
        form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')
            if form_competencia.is_valid():
                respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                respuesta.pregunta = pregunta # asociarloo a la pregunta
                respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                respuesta.save() #  y ahora si lo guarda posta.

        try:
            informe_anual = InformeAnual.objects.get(empleado=empleado, lider=empleado.lider, periodo__year=today.year) 

        except InformeAnual.DoesNotExist:
                informe_anual =  InformeAnual.objects.create(empleado=empleado, lider=empleado.lider, periodo=evaluacion.fecha)
                evaluacion.informe_anual=informe_anual
                evaluacion.save()
                informe_anual.save()

        return redirect('evaluaciones_empleado')
        
    greeting = {
            'empleado': empleado,
            'forms_competencia':forms_competencia,          
        }
    
    template_name = 'indicadores/empleado/evaluacion_empleado.html'
    return render(request, template_name, greeting)


############################# RESPONDER EVALUACIONES DE LOS EMPLEADOS POR LOS LIDERES ADMINISTRADORES

#VER INFORME DE TODOS LOS EMPLEADOS POR EL LIDER ADMIN
def ver_evaluacion_admin_general(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/admin/ver_evaluacion_admin_general.html', greeting)

## que vea los empleados
def evaluaciones_admin_general(request):         

    evaluaciones = Evaluacion.objects.all()

    greeting = {}
    greeting['heading'] = "Evaluaciones Desempeño"
    greeting['pageview'] = "Empleados"
    greeting['evaluaciones'] = evaluaciones  

    template_name = 'indicadores/admin/evaluaciones_admin_general.html'
    return render(request, template_name, greeting)

## que vea los informas anuales
def evaluaciones_admin_anual(request):         

    evaluaciones = InformeAnual.objects.all()

    greeting = {}
    greeting['heading'] = "Evaluaciones Desempeño"
    greeting['pageview'] = "Promedio Anual"
    greeting['evaluaciones'] = evaluaciones

    template_name = 'indicadores/admin/evaluaciones_admin_anual.html'
    return render(request, template_name, greeting)

#VER INFORME DEL EMPLEADO POR EL LIDER ADMIN
def ver_evaluacion_admin_empleado(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/admin/ver_evaluacion_admin_empleado.html', greeting)

## que vea los empleados
def evaluaciones_admin(request):         

    lider  = request.user.lider
    evaluaciones = Evaluacion.objects.filter(empleado__lider = lider)

    greeting = {}
    greeting['heading'] = "Evaluaciones Desempeño"
    greeting['pageview'] = "Equipo De Trabajo"
    greeting['lider'] = lider 
    greeting['evaluaciones'] = evaluaciones  

    template_name = 'indicadores/admin/evaluaciones_admin.html'
    return render(request, template_name, greeting)

def responder_evaluacion_empleado(request, id):
    lider = request.user.lider
    
    evaluacion = Evaluacion.objects.get(id=id) 

    # ver si la evaluacion tiene objetivos
    respuestas_objetivo = RespuestaObjetivo.objects.filter(evaluacion=evaluacion)

    if respuestas_objetivo:
        
        forms_objetivo = []                   
        
        for respuestaobjetivo in respuestas_objetivo:
            form_objetivo = RespuestaObjetivoEvaluadorForm(instance=respuestaobjetivo, prefix=f'respuestaobjetivo_{respuestaobjetivo.pk}')
            forms_objetivo.append((respuestaobjetivo, form_objetivo))

        if request.method == 'POST' and 'objetivo' in request.POST:

            for respuestaobjetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadorForm(request.POST, instance=respuestaobjetivo, prefix=f'respuestaobjetivo_{respuestaobjetivo.pk}')
                if form_objetivo.is_valid():
                    form_objetivo.save()

            return redirect('evaluacion_competencia_admin', id=evaluacion.id)
    
        greeting = {}
        greeting['lider'] = lider
        greeting['evaluacion'] = evaluacion   
        greeting['respuestas_objetivo'] = respuestas_objetivo   
        greeting['forms_objetivo'] = forms_objetivo
    
    else:
        # si la evaluacion no tiene objetivos
        respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)
        print(respuestas_competencia)
        if respuestas_competencia:
            
        ######## formularios de comptencias get
            forms_competencia = []
        
            for pregunta in respuestas_competencia:
                form_competencia = RespuestaCompetenciaEvaluadorForm(instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
                forms_competencia.append((pregunta, form_competencia))
            
        if 'competencia' in request.POST:
            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST, instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    form_competencia.save() 
            return redirect('evaluaciones_admin')
        
        greeting = {}
        greeting['lider'] = lider
        greeting['evaluacion'] = evaluacion   
        greeting['forms_competencia'] = forms_competencia

    template_name = 'indicadores/admin/evaluacion_admin.html'
    return render(request, template_name, greeting)

def evaluacion_competencia_admin(request, id):

    lider = request.user.lider
    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)
        
    ######## formularios de comptencias get
    forms_competencia = []

    for pregunta in respuestas_competencia:
        form_competencia = RespuestaCompetenciaEvaluadorForm(instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST, instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
            if form_competencia.is_valid():
                form_competencia.save() 
        return redirect('evaluaciones_admin')
        
    context = {
            'lider': lider,
            'forms_competencia':forms_competencia,    
            'evaluacion':evaluacion,       
        }
    
    template_name = 'indicadores/admin/evaluacion_admin.html'
    return render(request, template_name, context)


############################# EVALUACIONES DE LOS LIDERES ADMINISTRADORES

# VER INFORME DEL LIDER ADMIN
def ver_evaluacion_admin(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/admin/ver_evaluacion_admin.html', greeting)

# el lider ve las evaluacionoes de sii mismo 
def admin_evaluaciones(request):

    lider = request.user.lider
    director  = request.user.lider.director
    evaluaciones = Evaluacion.objects.filter(lider=lider, director=director)
    anio_actual = today.year

    try:
        evaluaciones_anuales  = Evaluacion.objects.filter(lider=lider, director=director, fecha__year=anio_actual)
        non_none_resultados = [evaluacion.resultado_final for evaluacion in evaluaciones_anuales if evaluacion.resultado_final is not None]

        if non_none_resultados:
            # Calcule el resultado anual sólo si hay evaluaciones disponibles para el año.
            promedio_anual = sum(non_none_resultados) / len(evaluaciones_anuales)
        else:
            promedio_anual = 0  # Establece un valor por defecto '0' maneja el escenario apropiadamente.

        # Guarda el 'promedio_anual' calculado en el atributo 'resultado'.
        informe_anual = InformeAnual.objects.get(lider=lider, director=director, periodo__year=anio_actual)
        informe_anual.resultado = promedio_anual
        informe_anual.save()

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['promedio_anual'] = promedio_anual  
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    except InformeAnual.DoesNotExist:

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    template_name = 'indicadores/admin/admin_evaluaciones.html'
    return render(request, template_name, greeting)

## que el lider responda su propio examen
# el lider empieza a autoevaluarse
def admin_nueva_evaluacion(request):
        
    # si es operativo que no muestre objetivos sino competencias
    lider = request.user.lider
    objetivos = lider.cargo.objetivos.all() 

    if request.method == "GET": # crear una nueva instancia al empezar 
        evaluacion = Evaluacion.objects.create(lider=lider, director=lider.director)
        evaluacion.save()   

    if objetivos:     ## si el empleado tiene objetivos asociados

        ## crear nueva evaluacion con formulario de objetivos
        ### formulario de objetivos para el cargo del empleado
        forms_objetivo = [] 
        for objetivo in objetivos:
            form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST or None, prefix=f'objetivo_{objetivo.pk}')
            forms_objetivo.append((objetivo, form_objetivo))
            
        if 'objetivo' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id')
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)

            for objetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST, prefix=f'objetivo_{objetivo.pk}')
                if form_objetivo.is_valid():
                    respuesta = form_objetivo.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.objetivo = objetivo # asociarloo al objetivo
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.

            try:
                informe_anual = InformeAnual.objects.get(lider=lider, director=lider.director, periodo__year=today.year)
            except InformeAnual.DoesNotExist:
                informe_anual =  InformeAnual.objects.create(lider=lider, director=lider.director, periodo=evaluacion.fecha)
                evaluacion.informe_anual=informe_anual

            return redirect('admin_evaluacion_competencia', id=evaluacion.id)
            
        greeting = {
            'lider': lider,
            'forms_objetivo':forms_objetivo,      
            'evaluacion':evaluacion    
        }
    
    else:         
        ## si el lider NO TIENE objetivos asociados       
        
        ######## formularios de comptencias get
        preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=lider.nivel_administrativo)
        
        forms_competencia = []
        
        for pregunta in preguntas:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
            forms_competencia.append((pregunta, form_competencia))
        
        if 'competencia' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id', None)
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)

            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.pregunta = pregunta # asociarloo a la pregunta
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.
            return redirect('admin_evaluaciones')
            
        greeting = {
                'lider': lider,
                'forms_competencia':forms_competencia, 
                'evaluacion':evaluacion    
            }

    template_name = 'indicadores/admin/admin_evaluacion.html'
    return render(request, template_name, greeting)

def admin_evaluacion_competencia(request, id):
    
    lider = request.user.lider
    evaluacion = Evaluacion.objects.get(id=id)
    preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=lider.nivel_administrativo)
    forms_competencia = []
    for pregunta in preguntas:
        form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')
            if form_competencia.is_valid():
                respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                respuesta.pregunta = pregunta # asociarloo a la pregunta
                respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                respuesta.save() #  y ahora si lo guarda posta.
        return redirect('admin_evaluaciones')
        
    context = {
            'lider': lider,
            'forms_competencia':forms_competencia,          
        }
    
    template_name = 'indicadores/admin/admin_evaluacion.html'
    return render(request, template_name, context)


############################# EVALUACIONES DE LOS EMPLEADOS POR LOS LIDERES

#VER INFORME DEL EMPLEADO POR EL LIDER
def ver_evaluacion_lider_empleado(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/lider/ver_evaluacion_lider_empleado.html', greeting)

## que vea los empleados por el lider
def evaluaciones_lider(request):         

    lider  = request.user.lider
    evaluaciones = Evaluacion.objects.filter(empleado__lider = lider)

    greeting = {}
    greeting['heading'] = "Evaluaciones Desempeño"
    greeting['pageview'] = "Equipo De Trabajo"
    greeting['lider'] = lider 
    greeting['evaluaciones'] = evaluaciones  

    template_name = 'indicadores/lider/evaluaciones_lider.html'
    return render(request, template_name, greeting)

## que el lider responda las evaluaciones de los empleados
def responder_evaluacion(request, id):

    lider = request.user.lider
    evaluacion = Evaluacion.objects.get(id=id) 
    # ver si la evaluacion tiene objetivos
    respuestas_objetivo = RespuestaObjetivo.objects.filter(evaluacion=evaluacion)

    if respuestas_objetivo:
        
        forms_objetivo = []                   
        
        for respuestaobjetivo in respuestas_objetivo:
            form_objetivo = RespuestaObjetivoEvaluadorForm(instance=respuestaobjetivo, prefix=f'respuestaobjetivo_{respuestaobjetivo.pk}')
            forms_objetivo.append((respuestaobjetivo, form_objetivo))

        if request.method == 'POST' and 'objetivo' in request.POST:

            for respuestaobjetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadorForm(request.POST, instance=respuestaobjetivo, prefix=f'respuestaobjetivo_{respuestaobjetivo.pk}')
                if form_objetivo.is_valid():
                    form_objetivo.save()

            return redirect('evaluacion_competencia_lider', id=evaluacion.id)
    
        greeting = {}
        greeting['lider'] = lider
        greeting['evaluacion'] = evaluacion   
        greeting['respuestas_objetivo'] = respuestas_objetivo   
        greeting['forms_objetivo'] = forms_objetivo
    
    else:
        # si la evaluacion no tiene objetivos
        respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

        if respuestas_competencia:
            
        ######## formularios de comptencias get
            forms_competencia = []
        
            for pregunta in respuestas_competencia:
                form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST or None, instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
                forms_competencia.append((pregunta, form_competencia))
            
        if request.method == 'POST' and 'competencia' in request.POST:
            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST, instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    form_competencia.save() 
                else:
                    return HttpResponse(f'{form_competencia.errors}')
                    
            return redirect('evaluaciones_lider')
        
        greeting = {}
        greeting['lider'] = lider
        greeting['evaluacion'] = evaluacion   
        greeting['forms_competencia'] = forms_competencia

    template_name = 'indicadores/lider/evaluacion_lider.html'
    return render(request, template_name, greeting)


def evaluacion_competencia_lider(request, id):

    lider = request.user.lider
    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)
        
    ######## formularios de comptencias get
    forms_competencia = []

    for pregunta in respuestas_competencia:
        form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST or None,instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if request.method == 'POST' and 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST,instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
            print(form_competencia)
            if form_competencia.is_valid():
                form_competencia.save() 
            else:
                return HttpResponse(f'{form_competencia.errors}')
            
        return redirect('evaluaciones_lider')
        
    greeting = {
            'lider': lider,
            'forms_competencia':forms_competencia,    
            'evaluacion':evaluacion,       
        }
    
    template_name = 'indicadores/lider/evaluacion_lider.html'
    return render(request, template_name, greeting)


############################# EVALUACIONES DE LOS LIDERES

#VER INFORME DEL LIDER
def ver_evaluacion_lider(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    # promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    promedio_por_competencia_evaluado = []

    for item in promedio_competencia_evaluado:
        competencia_id = item['pregunta__competencia']
        promedio = item['promedio'] if item['promedio'] is not None else 0
        try:
            competencia = Competencia.objects.get(pk=competencia_id)
        except Competencia.DoesNotExist:
            competencia = None
        promedio_por_competencia_evaluado.append((competencia, promedio))
        
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/lider/ver_evaluacion_lider.html', greeting)

# el lider ve las evaluaciones de si mismo 
def lider_evaluaciones(request):

    lider = request.user.lider
    director  = request.user.lider.director
    evaluaciones = Evaluacion.objects.filter(lider=lider,director=director)
    anio_actual = today.year

    try:
        evaluaciones_anuales  = Evaluacion.objects.filter(lider=lider, director=director, fecha__year=anio_actual)
        non_none_resultados = [evaluacion.resultado_final for evaluacion in evaluaciones_anuales if evaluacion.resultado_final is not None]

        if non_none_resultados:
            # Calcule el resultado anual sólo si hay evaluaciones disponibles para el año.
            promedio_anual = sum(non_none_resultados) / len(evaluaciones_anuales)
        else:
            promedio_anual = 0  # Establece un valor por defecto '0' maneja el escenario apropiadamente.

        # Guarda el 'promedio_anual' calculado en el atributo 'resultado'.
        informe_anual = InformeAnual.objects.get(lider=lider, director=director, periodo__year=anio_actual)
        informe_anual.resultado = promedio_anual
        informe_anual.save()

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['promedio_anual'] = promedio_anual  
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    except InformeAnual.DoesNotExist:

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    template_name = 'indicadores/lider/lider_evaluaciones.html'
    return render(request, template_name, greeting)

# el lider empieza a autoevaluarse
def lider_nueva_evaluacion(request):

    # si es operativo que no muestre objetivos sino competencias
    lider = request.user.lider

    if request.method == "GET": # crear una nueva instancia al empezar 
        evaluacion = Evaluacion.objects.create(lider=lider, director=lider.director)
        evaluacion.save()

    objetivos = lider.cargo.objetivos.all() 

    if objetivos:     ## si el empleado tiene objetivos asociados

        ## crear nueva evaluacion con formulario de objetivos
        ### formulario de objetivos para el cargo del empleado
        forms_objetivo = [] 
        for objetivo in objetivos:
            form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST or None, prefix=f'objetivo_{objetivo.pk}')
            forms_objetivo.append((objetivo, form_objetivo))
            
        if 'objetivo' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id')
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)
            for objetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST, prefix=f'objetivo_{objetivo.pk}')
                if form_objetivo.is_valid():
                    respuesta = form_objetivo.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.objetivo = objetivo # asociarloo al objetivo
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.
            try:
                informe_anual = InformeAnual.objects.get(lider=lider, director=lider.director, periodo__year=today.year)
            except InformeAnual.DoesNotExist:
                informe_anual =  InformeAnual.objects.create(lider=lider, director=lider.director, periodo=evaluacion.fecha)
                evaluacion.informe_anual=informe_anual    
                    
            return redirect('lider_evaluacion_competencia', id=evaluacion.id)
                
            """            
            AQUI hay que redirecionar al empleado al fomrulario de comptenencias que es otra vista
            
            """
        greeting = {
            'lider': lider,
            'forms_objetivo':forms_objetivo,      
            'evaluacion':evaluacion    
        }
    
    else:         
        ## si el lider NO TIENE objetivos asociados       
        
        ######## formularios de competencias get
        preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=lider.nivel_administrativo)
        
        forms_competencia = []
        
        for pregunta in preguntas:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
            forms_competencia.append((pregunta, form_competencia))
        
        if 'competencia' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id', None)
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)

            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.pregunta = pregunta # asociarloo a la pregunta
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.

            return redirect('lider_evaluaciones')
            
        greeting = {
                'lider': lider,
                'forms_competencia':forms_competencia, 
                'evaluacion':evaluacion    
            }

    template_name = 'indicadores/lider/lider_evaluacion.html'
    return render(request, template_name, greeting)


def lider_evaluacion_competencia(request, id):
    
    lider = request.user.lider
    evaluacion = Evaluacion.objects.get(id=id)
    preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=lider.nivel_administrativo)

    forms_competencia = []
    for pregunta in preguntas:
        form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')

            if form_competencia.is_valid():
                respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                respuesta.pregunta = pregunta # asociarloo a la pregunta
                respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                respuesta.save() #  y ahora si lo guarda posta.

        return redirect('lider_evaluaciones')
        
    greeting = {
            'lider': lider,
            'forms_competencia':forms_competencia,          
        }
    
    template_name = 'indicadores/lider/lider_evaluacion.html'
    return render(request, template_name, greeting)

############################# EVALUACIONES DE LOS LIDERES POR LOS DIRECTORES

#VER INFORME DEL LIDER POR EL DIRECTOR
def ver_evaluacion_director_lider(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/director/ver_evaluacion_director_lider.html', greeting)

# el director ve las evaluaciones de los lideres
def director_evaluaciones(request):

    director = request.user.director
    lider = request.user.lider
    user = request.user

    evaluaciones_del_director = Evaluacion.objects.filter(director__user=user)
    evaluaciones_del_lider = Evaluacion.objects.filter(lider__user=user)

    # Combinar las evaluaciones de director y líder en una sola lista
    evaluaciones = evaluaciones_del_director | evaluaciones_del_lider
    # evaluaciones = Evaluacion.objects.filter(director=director)
    
    greeting = {}
    greeting['heading'] = "Evaluación Desempeño"
    greeting['pageview'] = "Equipo De Trabajo"
    greeting['director'] = director
    greeting['evaluaciones'] = evaluaciones

    template_name = 'indicadores/director/director_evaluaciones.html'
    return render(request, template_name, greeting)

# Empieza a responder la evaluacion del lider por el director
def evaluacion_objetivo_director(request, id):

    director = request.user.director
    evaluacion = Evaluacion.objects.get(id=id) 
    # ver si la evaluacion tiene objetivos
    respuestas_objetivo = RespuestaObjetivo.objects.filter(evaluacion=evaluacion)

    if respuestas_objetivo:
        
        forms_objetivo = []                   
        
        for respuestaobjetivo in respuestas_objetivo:
            form_objetivo = RespuestaObjetivoEvaluadorForm(instance=respuestaobjetivo, prefix=f'respuestaobjetivo_{respuestaobjetivo.pk}')
            forms_objetivo.append((respuestaobjetivo, form_objetivo))

        if request.method == 'POST':
            for respuestaobjetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadorForm(request.POST, instance=respuestaobjetivo, prefix=f'respuestaobjetivo_{respuestaobjetivo.pk}')
                if form_objetivo.is_valid():
                    form_objetivo.save()
            return redirect('evaluacion_competencia_director', id=evaluacion.id)
            
        greeting = {
                'director': director,
                'forms_objetivo': forms_objetivo,          
                'evaluacion': evaluacion,          
            }
    else:
        # si la evaluacion no tiene objetivos
        respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)
        if respuestas_competencia:
            
        ######## formularios de comptencias get
            forms_competencia = []
        
            for pregunta in respuestas_competencia:
                form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST or None,instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
                forms_competencia.append((pregunta, form_competencia))
            
        if 'competencia' in request.POST:
            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST,instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    form_competencia.save()
                else:
                    return HttpResponse(f'{form_competencia.errors}')
 
            return redirect('director_evaluaciones')
            
        greeting = {
                'director': director,
                'forms_comptencia':forms_competencia,          
            }
    
    template_name = 'indicadores/director/evaluacion_director.html'
    return render(request, template_name, greeting)

def evaluacion_competencia_director(request, id):
    
    director = request.user.director
    evaluacion = Evaluacion.objects.get(id=id)
    
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)
        
    ######## formularios de competencias get
    forms_competencia = []

    for pregunta in respuestas_competencia:
        form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST or None,instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadorForm(request.POST,instance=pregunta, prefix=f'pregunta_{pregunta.pk}')
            if form_competencia.is_valid():
                form_competencia.save() 
            else:
                return HttpResponse(f'{form_competencia.errors}')

        return redirect('director_evaluaciones')
        
    greeting = {
            'director': director,
            'forms_competencia':forms_competencia,      
            'evaluacion': evaluacion,      
        }
    
    template_name = 'indicadores/director/evaluacion_director.html'
    return render(request, template_name, greeting)


############################# EVALUACIONES DE LOS DIRECTORES

#VER INFORME DEL DIRECTOR
def ver_evaluacion_director(request, id):
    
    promedio_total = 0
    competencias = []
    subcompetencias = []

    evaluacion = Evaluacion.objects.get(id=id)
    respuestas_competencia = RespuestaCompetencia.objects.filter(evaluacion=evaluacion)

    promedio_competencia_evaluado = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluado'))
    promedio_por_competencia_evaluado = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluado]
    
    promedio_competencia_evaluador = RespuestaCompetencia.objects.filter(evaluacion=evaluacion).values('pregunta__competencia').annotate(promedio=Avg('porcentaje_evaluador'))
    promedio_por_competencia_evaluador = [(Competencia.objects.get(pk=item['pregunta__competencia']), item['promedio']) if item['promedio'] is not None else (Competencia.objects.get(pk=item['pregunta__competencia']), 0) for item in promedio_competencia_evaluador]

    if evaluacion.get_porcentaje_competencias_evaluado != 0 and evaluacion.get_porcentaje_competencias_evaluador != 0:
        promedio_total_competencia = (evaluacion.get_porcentaje_competencias_evaluador * 0.8) + (evaluacion.get_porcentaje_competencias_evaluado * 0.2)
    elif evaluacion.get_porcentaje_competencias_evaluado != 0:
        promedio_total_competencia = evaluacion.get_porcentaje_competencias_evaluado * 0.2
    else:
        promedio_total_competencia = 0

    if evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0 and evaluacion.get_porcentaje_respuestas_objetivo_evaluador != 0:
        promedio_total_objetivo = (evaluacion.get_porcentaje_respuestas_objetivo_evaluador * 0.8) + (evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2)
    elif evaluacion.get_porcentaje_respuestas_objetivo_evaluado != 0:
        promedio_total_objetivo = evaluacion.get_porcentaje_respuestas_objetivo_evaluado * 0.2   
    else:
        promedio_total_objetivo = 0

    if promedio_total_competencia != 0 and promedio_total_objetivo != 0:
        promedio_total = promedio_total_competencia * 0.2 + promedio_total_objetivo * 0.8
    elif promedio_total_competencia != 0:
        promedio_total = promedio_total_competencia
    elif promedio_total_objetivo != 0:
        promedio_total = promedio_total_objetivo

    evaluacion.porcentaje_objetivos_evaluado = evaluacion.get_porcentaje_respuestas_objetivo_evaluado
    evaluacion.porcentaje_objetivos_evaluador = evaluacion.get_porcentaje_respuestas_objetivo_evaluador
    evaluacion.porcentaje_competencias_evaluado = evaluacion.get_porcentaje_competencias_evaluado
    evaluacion.porcentaje_competencias_evaluador = evaluacion.get_porcentaje_competencias_evaluador
    evaluacion.resultado_final = promedio_total
    evaluacion.save()

    for respuesta_competencia in respuestas_competencia:
        if respuesta_competencia.pregunta.competencia not in competencias:
            competencias.append(respuesta_competencia.pregunta.competencia)
        if respuesta_competencia.pregunta not in subcompetencias:
            subcompetencias.append(respuesta_competencia.pregunta)

    greeting = {
        'evaluacion': evaluacion,
        'competencias': competencias,
        'subcompetencias': subcompetencias,
        'promedio_por_competencia_evaluado': promedio_por_competencia_evaluado,
        'promedio_por_competencia_evaluador': promedio_por_competencia_evaluador,
        'promedio_total' : promedio_total
    }

    return render(request, 'indicadores/director/ver_evaluacion_director.html', greeting)


# el director ve las evaluaciones de si mismo 
def evaluaciones_director(request):

    lider = request.user.lider
    director  = request.user.lider.director
    evaluaciones = Evaluacion.objects.filter(lider=lider,director=director)
    anio_actual = today.year

    try:
        evaluaciones_anuales  = Evaluacion.objects.filter(lider=lider, director=director, fecha__year=anio_actual)
        non_none_resultados = [evaluacion.resultado_final for evaluacion in evaluaciones_anuales if evaluacion.resultado_final is not None]

        if non_none_resultados:
            # Calcule el resultado anual sólo si hay evaluaciones disponibles para el año.
            promedio_anual = sum(non_none_resultados) / len(evaluaciones_anuales)
        else:
            promedio_anual = 0  # Establece un valor por defecto '0' maneja el escenario apropiadamente.

        # Guarda el 'promedio_anual' calculado en el atributo 'resultado'.
        informe_anual = InformeAnual.objects.get(lider=lider, director=director, periodo__year=anio_actual)
        informe_anual.resultado = promedio_anual
        informe_anual.save()

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['promedio_anual'] = promedio_anual  
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    except InformeAnual.DoesNotExist:

        greeting = {}
        greeting['evaluaciones'] = evaluaciones   
        greeting['anio'] = anio_actual
        greeting['heading'] = "Evaluación Desempeño"
        greeting['pageview'] = "Personal"

    template_name = 'indicadores/director/evaluaciones_director.html'
    return render(request, template_name, greeting)

# el director empieza a autoevaluarse
def director_nueva_evaluacion(request):

    # si es operativo que no muestre objetivos sino competencias
    lider = request.user.lider

    if request.method == "GET": # crear una nueva instancia al empezar 
        evaluacion = Evaluacion.objects.create(lider=lider, director=lider.director)
        evaluacion.save()

    objetivos = lider.cargo.objetivos.all() 

    if objetivos:     ## si el empleado tiene objetivos asociados

        ## crear nueva evaluacion con formulario de objetivos
        ### formulario de objetivos para el cargo del empleado
        forms_objetivo = [] 
        for objetivo in objetivos:
            form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST or None, prefix=f'objetivo_{objetivo.pk}')
            forms_objetivo.append((objetivo, form_objetivo))
            
        if 'objetivo' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id')
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)
            for objetivo, form_objetivo in forms_objetivo:
                form_objetivo = RespuestaObjetivoEvaluadoForm(request.POST, prefix=f'objetivo_{objetivo.pk}')
                if form_objetivo.is_valid():
                    respuesta = form_objetivo.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.objetivo = objetivo # asociarloo al objetivo
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.
            try:
                informe_anual = InformeAnual.objects.get(lider=lider, director=lider.director, periodo__year=today.year)
            except InformeAnual.DoesNotExist:
                informe_anual =  InformeAnual.objects.create(lider=lider, director=lider.director, periodo=evaluacion.fecha)
                evaluacion.informe_anual=informe_anual    
                    
            return redirect('director_evaluacion_competencia', id=evaluacion.id)
                
            """            
            AQUI hay que redirecionar al empleado al fomrulario de comptenencias que es otra vista
            
            """
        greeting = {
            'lider': lider,
            'forms_objetivo':forms_objetivo,      
            'evaluacion':evaluacion    
        }
    
    else:         
        ## si el director NO TIENE objetivos asociados       
        
        ######## formularios de competencias get
        preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=lider.nivel_administrativo)
        
        forms_competencia = []
        
        for pregunta in preguntas:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
            forms_competencia.append((pregunta, form_competencia))
        
        if 'competencia' in request.POST:
            evaluacion_id = request.POST.get('evaluacion_id', None)
            evaluacion = Evaluacion.objects.get(id=evaluacion_id)

            for pregunta, form_competencia in forms_competencia:
                form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')
                if form_competencia.is_valid():
                    respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                    respuesta.pregunta = pregunta # asociarloo a la pregunta
                    respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                    respuesta.save() #  y ahora si lo guarda posta.

            return redirect('evaluaciones_director')
            
        greeting = {
                'lider': lider,
                'forms_competencia':forms_competencia, 
                'evaluacion':evaluacion    
            }

    template_name = 'indicadores/director/director_evaluacion.html'
    return render(request, template_name, greeting)


def director_evaluacion_competencia(request, id):
    
    lider = request.user.lider
    evaluacion = Evaluacion.objects.get(id=id)
    preguntas = Pregunta.objects.filter(competencia__nivel_administrativo=lider.nivel_administrativo)

    forms_competencia = []
    for pregunta in preguntas:
        form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST or None, prefix=f'pregunta_{pregunta.pk}')
        forms_competencia.append((pregunta, form_competencia))
    
    if 'competencia' in request.POST:
        for pregunta, form_competencia in forms_competencia:
            form_competencia = RespuestaCompetenciaEvaluadoForm(request.POST, prefix=f'pregunta_{pregunta.pk}')

            if form_competencia.is_valid():
                respuesta = form_competencia.save(commit=False) # que hace esto, lo preguarda para
                respuesta.pregunta = pregunta # asociarloo a la pregunta
                respuesta.evaluacion = evaluacion # asociarlo a la evaluacion
                respuesta.save() #  y ahora si lo guarda posta.

        return redirect('evaluaciones_director')
        
    greeting = {
            'lider': lider,
            'forms_competencia':forms_competencia, 
            'evaluacion':evaluacion          
        }
    
    template_name = 'indicadores/director/director_evaluacion.html'
    return render(request, template_name, greeting)