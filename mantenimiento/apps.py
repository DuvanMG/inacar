from django.apps import AppConfig
from django.conf import settings
import os

class MantenimientoConfig(AppConfig):
    name = 'mantenimiento'
    path = os.path.join(settings.BASE_DIR, 'mantenimiento')