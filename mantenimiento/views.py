from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.views import View
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
# from .forms import preguntaFrom, EvaluacionFrom
from indicadores.models import Lider
    
# Ingresar Grupo de Lideres
class GrupoLideresView(LoginRequiredMixin,View):
    def get(self,request):

        greeting = {}
        greeting['heading'] = "Grupo Lideres"
        greeting['pageview'] = "Historial"
        greeting['Lideres'] = Lider.objects.all()
        print(greeting['Lideres'])
        return render (request,'mantenimiento/GrupoLider.html',greeting)
    
#     def post(self,request):
        
#         # Crear Historial Evaluacion de Indicadores
#         Evaluacion = EvaluacionFrom(request.POST)
#         if Evaluacion.is_valid:
#             Evaluacion.save()
#             messages.success(request, "Su respuesta se guardo correctamente")
#             return redirect('EvaluacionIndicadores')