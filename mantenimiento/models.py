from django.db import models
from indicadores.models import Lider

# # Create your models here.
# class Desempeño(models.Model):
#     Id_Desempeño = models.AutoField(auto_created=True, primary_key=True)
#     desempeñonombre = models.CharField(max_length=230, null=False)
    
#     def __str__(self):
#         return self.desempeñonombre
    
# class pregunta(models.Model):
#     Id_pregunta = models.AutoField(auto_created=True, primary_key=True)
#     niveladministrativonombre = models.ForeignKey(NivelesAdministrativos, on_delete=models.CASCADE, null=False, blank=False)
#     desempeñonombre = models.ForeignKey(Desempeño, on_delete=models.CASCADE, null=False, blank=False)
#     titulo = models.CharField(max_length=230, null=False)
#     descripcion = models.CharField(max_length=530, null=False)
#     porcentaje = models.IntegerField()
#     fecha = models.DateField()

#     def __str__(self):
#         return self.titulo
    
# class Evaluacion(models.Model):
#     Id_Evaluacion = models.AutoField(auto_created=True, primary_key=True)
#     empleadonombre = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
#     pregunta = models.ForeignKey(pregunta, on_delete=models.CASCADE, null=False, blank=False)
#     respuesta = models.IntegerField()
#     observacion = models.CharField(max_length=230, null=True)
#     fecha = models.DateField(auto_now_add=True)

#     def __str__(self):
#         return 'Nombre del evaluado: ' + str(self.empleadonombre)
       
# class EvaluacionTemporales(models.Model):
#     Id_Evaluacion = models.AutoField(auto_created=True, primary_key=True)
#     empleadonombre = models.IntegerField()
#     pregunta = models.IntegerField()
#     respuesta = models.IntegerField()
#     observacion = models.CharField(max_length=230, null=True)
#     fecha = models.DateField(auto_now_add=True)

#     def __str__(self):
#         return str(self.empleadonombre)