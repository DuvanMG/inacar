from django import forms
from django.forms import ModelForm
from .models import CreacionCapacitacion, EmpleadoCapacitacionesInternas, EmpleadoCapacitacionesExternas

class CreacionCapacitacionFrom(ModelForm):
    class Meta:
        model = CreacionCapacitacion
        fields = ['NombreCapacitacion','NombreCapacitador','FechaCapacitacion','NombresAreas','LinkEvaluacion']

class EmpleadoCapacitacionesInternasFrom(ModelForm):
    class Meta:
        model = EmpleadoCapacitacionesInternas
        fields = ['NombreEmpleado','NombresCapacitaciones','NombreArea','Nota']

class EmpleadoCapacitacionesExternasFrom(ModelForm):
    class Meta:
        model = EmpleadoCapacitacionesExternas
        fields = ['NombreEmpleado','NombreInstitucion','NombreCapacitacion','NombreCapacitador','FechaCapacitacion','NombreArea']