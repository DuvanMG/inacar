from django.apps import AppConfig
from django.conf import settings
import os

class CapacitacionesConfig(AppConfig):
    name = 'capacitaciones'
    path = os.path.join(settings.BASE_DIR, 'capacitaciones')