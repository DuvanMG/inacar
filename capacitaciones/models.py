from django.db import models

# Create your models here.
class CreacionCapacitacion(models.Model):
    Id_CreacionCapacitacion = models.AutoField(auto_created=True, primary_key=True)
    NombreCapacitacion = models.CharField(max_length=230, null=False)
    NombreCapacitador = models.CharField(max_length=230, null=False)
    FechaCapacitacion = models.CharField(max_length=230, null=False)
    NombresAreas = models.CharField(max_length=999, null=False)
    LinkEvaluacion = models.CharField(max_length=530, null=False)

    def __str__(self):
        return self.NombreCapacitacion
    
class EmpleadoCapacitacionesInternas(models.Model):
    Id_Empleadocapacitacioninterna = models.AutoField(auto_created=True, primary_key=True)
    NombreEmpleado = models.CharField(max_length=230, null=False)
    NombresCapacitaciones = models.ForeignKey(CreacionCapacitacion, on_delete=models.CASCADE, null=False, blank=False)
    NombreArea = models.CharField(max_length=230, null=False)
    Nota = models.CharField(max_length=230, null=False)

class EmpleadoCapacitacionesExternas(models.Model):
    Id_EmpleadocapacitacionExterna = models.AutoField(auto_created=True, primary_key=True)
    NombreEmpleado = models.CharField(max_length=230, null=False)
    NombreInstitucion = models.CharField(max_length=230, null=False)
    NombreCapacitacion = models.CharField(max_length=230, null=False)
    NombreCapacitador = models.CharField(max_length=230, null=False)
    FechaCapacitacion = models.CharField(max_length=230, null=False)
    NombreArea = models.CharField(max_length=230, null=False)

