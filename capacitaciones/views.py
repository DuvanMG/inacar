from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.views import View
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CreacionCapacitacionFrom, EmpleadoCapacitacionesInternasFrom, EmpleadoCapacitacionesExternasFrom
from .models import CreacionCapacitacion, EmpleadoCapacitacionesInternas, EmpleadoCapacitacionesExternas

####### ADMIN
# Ingresar Capacitaciones Interna 
class CreacionCapacitacionView(LoginRequiredMixin,View):
    def get(self,request):
        greeting = {}
        greeting['heading'] = "Creación Capacitaciones"
        greeting['pageview'] = "Internas"
        greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
        
        return render (request,'capacitaciones/admin/creacion-capacitacion.html',greeting)
    
    def post(self,request):

        # Eliminar Datos Capacitacion Interno
        if "EliminarCapacitacion" in request.POST:
            id = request.POST['id']
            obj = CreacionCapacitacion.objects.filter(Id_CreacionCapacitacion=id).first()
            obj.delete()
            return HttpResponse()
        
        # Crear Datos Capacitacion Interno
        form = CreacionCapacitacionFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su capacitación se guardo correctamente")
            return redirect('creacion-capacitaciones')

# Actualizar Datos Capacitacion Interno
def UpdateCapacitacion(request,id):
    if request.method == 'POST':
        form = CreacionCapacitacion.objects.get(Id_CreacionCapacitacion=id)
        Capacitacion = CreacionCapacitacionFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('creacion-capacitaciones')
        
# Ingresar Capacitacion Personal Interno 
class InternaPersonalView(LoginRequiredMixin,View):
    def get(self , request):
        greeting = {}
        greeting['heading'] = "Personal Capacitado"
        greeting['pageview'] = "Interno"
        greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.all()
        return render (request,'capacitaciones/admin/capacitaciones-internas-personal.html',greeting)

# Actualizar Capacitacion Personal interno
def UpdatePersonalCapacitacionInterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=id)
        Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('capacitaciones-internas-personal')

# Ingresar Capacitacion Personal Externo              
class ExternaPersonalView(LoginRequiredMixin,View):
    def get(self , request):
        greeting = {}
        greeting['heading'] = "Personal Capacitado"
        greeting['pageview'] = "Externo"
        greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.all()
        return render (request,'capacitaciones/admin/capacitaciones-externas-personal.html',greeting) 
    
# Actualizar Capacitacion Personal Externo 
def UpdatePersonalCapacitacionExterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=id)
        Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('capacitaciones-externas-personal') 

##### Gestión ADMIN
# Ingresar Admin Capacitación Interno          
class AdminCapacitacionInternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.lider.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Interno"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
        greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreEmpleado)
        
        return render (request,'capacitaciones/admin/admincapacitacioninterna.html',greeting)
    
    def post(self,request):

        # Crear datos Admin Capacitación interno
        form = EmpleadoCapacitacionesInternasFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('admincapacitacioninterna')

# Actualizar datos Admin Capacitación interno
def UpdateAdminCapacitacionInterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=id)
        Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('admincapacitacioninterna')
                
# Ingresar Admin Capacitación Externo
class AdminCapacitacionExternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.lider.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Externo"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreEmpleado)

        return render (request,'capacitaciones/admin/admincapacitacionexterna.html',greeting)

    def post(self,request):

        # Crear datos Admin Capacitación externo
        form = EmpleadoCapacitacionesExternasFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('admincapacitacionexterna')
        
# Actualizar datos Admin Capacitación externo
def UpdateAdminCapacitacionExterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=id)
        Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('admincapacitacionexterna')


###### Gestión DIRECTOR
# Ingresar Director Capacitación Interno          
class DirectorCapacitacionInternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.lider.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Interno"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
        greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreEmpleado)
        
        return render (request,'capacitaciones/director/directorcapacitacioninterno.html',greeting)
    
    def post(self,request):

        # Crear datos Director Capacitación interno
        form = EmpleadoCapacitacionesInternasFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('directorcapacitacioninterno')

# Actualizar datos Director Capacitación interno
def UpdateDirectorCapacitacionInterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=id)
        Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('directorcapacitacioninterno')

# Ingresar Director Capacitación Externo
class DirectorCapacitacionExternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.lider.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Externo"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreEmpleado)

        return render (request,'capacitaciones/director/directorcapacitacionexterno.html',greeting)

    def post(self,request):

        # Crear datos Director Capacitación externo
        form = EmpleadoCapacitacionesExternasFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('directorcapacitacionexterno')
        
# Actualizar datos Director Capacitación externo
def UpdateDirectorCapacitacionExterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=id)
        Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid:
            Capacitacion.save()
            return redirect('directorcapacitacionexterno')


###### Gestión LIDER
# Ingresar lider Capacitación Interno          
class LiderCapacitacionInternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.lider.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Interno"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
        greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreEmpleado)
        
        return render (request,'capacitaciones/lider/lidercapacitacioninterno.html',greeting)
    
    def post(self,request):

        # Crear datos Lider Capacitación interno
        form = EmpleadoCapacitacionesInternasFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('lidercapacitacioninterno')

# Actualizar datos Lider Capacitación interno
def UpdateLiderCapacitacionInterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=id)
        Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('lidercapacitacioninterno')

# Ingresar Lider Capacitación Externo
class LiderCapacitacionExternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.lider.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Externo"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreEmpleado)

        return render (request,'capacitaciones/lider/lidercapacitacionexterno.html',greeting)

    def post(self,request):

        # Crear datos Lider Capacitación externo
        form = EmpleadoCapacitacionesExternasFrom(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('lidercapacitacionexterno')
        
# Actualizar datos Lider Capacitación externo
def UpdateLiderCapacitacionExterno(request,id):
    if request.method == 'POST':
        form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=id)
        Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid:
            Capacitacion.save()
            return redirect('lidercapacitacionexterno')
        

###### Gestión EMPLEADO        
# Ingresar Empleado Capacitación Externo          
class EmpleadoCapacitacionInternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.empleado.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Interno"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
        greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreEmpleado)
        
        return render (request,'capacitaciones/empleado/empleadocapacitacioninterno.html',greeting)
    
    def post(self,request):

        # Eliminar datos Empleado interno
        if "EliminarEmpleadoCapacitacionInterno" in request.POST:
            id = request.POST['id']
            obj = EmpleadoCapacitacionesInternas.objects.filter(Id_Empleadocapacitacioninterna=id).first()
            obj.delete()
            return HttpResponse()
        
        # Crear datos Empleado interno
        form = EmpleadoCapacitacionesInternasFrom(request.POST)
        if form.is_valid:
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('empleadocapacitacioninterno')

# Actualizar datos Empleado interno
def UpdateEmpleadoCapacitacionInterno(request,id):
    form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=id)
    if request.method == 'POST':
        Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('empleadocapacitacioninterno')
               
# Ingresar Empleado Capacitación Externo         
class EmpleadoCapacitacionExternoView(LoginRequiredMixin,View):
    def get(self , request):

        first_name = self.request.user.first_name
        last_name = self.request.user.last_name
        NombreEmpleado = first_name + ' ' + last_name
        cargo = request.user.empleado.cargo

        greeting = {}
        greeting['heading'] = "Capacitación"
        greeting['pageview'] = "Externo"
        greeting['NombreEmpleado'] = NombreEmpleado
        greeting['cargo'] = cargo
        greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreEmpleado)

        return render (request,'capacitaciones/empleado/empleadocapacitacionexterno.html',greeting)

    def post(self,request):

        # Eliminar datos Empleado externo
        if "EliminarEmpleadoCapacitacionExterno" in request.POST:
            id = request.POST['id']
            obj = EmpleadoCapacitacionesExternas.objects.filter(Id_EmpleadocapacitacionExterna=id).first()
            obj.delete()
            return HttpResponse()
        
        # Crear datos Empleado externo
        form = EmpleadoCapacitacionesExternasFrom(request.POST)
        if form.is_valid:
            form.save()
            messages.success(request, "Su asistencia se guardo correctamente")
            return redirect('empleadocapacitacionexterno')
        
# Actualizar datos Empleado externo        
def UpdateEmpleadoCapacitacionExterno(request,id):
    form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=id)
    if request.method == 'POST':
        Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
        if Capacitacion.is_valid():
            Capacitacion.save()
            return redirect('empleadocapacitacionexterno')  

# class PromotoraInternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Interno"
#         greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
#         greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreCompleto)
        
#         return render (request,'capacitaciones/empleadointernopromotora.html',greeting)
    
#     def post(self,request):

#         # Eliminar datos promotora interno
#         if "EliminarEmpleadoCapacitacionInterno" in request.POST:
#             id = request.POST['id']
#             obj = EmpleadoCapacitacionesInternas.objects.filter(Id_Empleadocapacitacioninterna=id).first()
#             obj.delete()
#             return HttpResponse()
        
#         # Crear datos promotora interno
#         form = EmpleadoCapacitacionesInternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadointernopromotora')

# # Actualizar datos promotoras interno
# def UpdatePromotoraInterno(request,Id_EmpleadocapacitacionInterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=Id_EmpleadocapacitacionInterna)
#         Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadointernopromotora')
               
# # Ingresar Promotora Externo         
# class PromotoraExternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Externo"
#         greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadoexternopromotora.html',greeting)

#     def post(self,request):

#         # Eliminar datos promotora externo
#         if "EliminarEmpleadoCapacitacionExterno" in request.POST:
#             id = request.POST['id']
#             obj = EmpleadoCapacitacionesExternas.objects.filter(Id_EmpleadocapacitacionExterna=id).first()
#             obj.delete()
#             return HttpResponse()
        
#         # Crear datos promotora externo
#         form = EmpleadoCapacitacionesExternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadoexternopromotora')
        
# # Actualizar datos promotora externo        
# def UpdatePromotoraExterno(request,Id_EmpleadocapacitacionExterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=Id_EmpleadocapacitacionExterna)
#         Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadoexternopromotora')  

# Ingresar Constructora Interno          
# class ConstructoraInternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Interno"
#         greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
#         greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadointernoconstructora.html',greeting)
    
#     def post(self,request):

#         # Crear datos constructora interno
#         form = EmpleadoCapacitacionesInternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadointernoconstructora') 

# # Actualizar datos constructora externo
# def UpdateConstructoraInterno(request,Id_Empleadocapacitacioninterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=Id_Empleadocapacitacioninterna)
#         Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadointernoconstructora')  
        
# # Ingresar Constructora Externo
# class ConstructoraExternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Externo"
#         greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadoexternoconstructora.html',greeting)

#     def post(self,request):

#         # Crear datos constructora externo
#         form = EmpleadoCapacitacionesExternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadoexternoconstructora') 

# # Actualizar datos constructora externo
# def UpdateConstructoraExterno(request,Id_EmpleadocapacitacionExterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=Id_EmpleadocapacitacionExterna)
#         Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadoexternoconstructora')  
        
# # Ingresar Inmobiliaria Interno          
# class InmobiliariaInternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Interno"
#         greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
#         greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadointernoinmobiliaria.html',greeting)
    
#     def post(self,request):

#         # Crear datos Inmobiliaria interno
#         form = EmpleadoCapacitacionesInternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadointernoinmobiliaria') 
        
# # Actualizar datos Inmobiliaria interno
# def UpdateInmobiliariaInterno(request,Id_EmpleadocapacitacionInterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=Id_EmpleadocapacitacionInterna)
#         Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadointernoinmobiliaria')
                
# # Ingresar Inmobiliaria Externo
# class InmobiliariaExternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Externo"
#         greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadoexternoinmobiliaria.html',greeting)

#     def post(self,request):

#         # Crear datos Inmobiliaria externo
#         form = EmpleadoCapacitacionesExternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadoexternoinmobiliaria')

# # Actualizar datos Inmobiliaria externo
# def UpdateInmobiliariaExterno(request,Id_EmpleadocapacitacionExterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=Id_EmpleadocapacitacionExterna)
#         Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadoexternoinmobiliaria')
        
# # Ingresar Unidades De Apoyo Interno          
# class UnidadesDeApoyoInternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Interno"
#         greeting['Capacitaciones'] = CreacionCapacitacion.objects.all()
#         greeting['EmpleadoCapacitacionesInternas'] = EmpleadoCapacitacionesInternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadointernounidadesdeapoyo.html',greeting)
    
#     def post(self,request):

#         # Crear datos Unidades De Apoyo interno
#         form = EmpleadoCapacitacionesInternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadointernounidadesdeapoyo')

# # Actualizar datos Unidades De Apoyo interno
# def UpdateUnidadesDeApoyoInterno(request,Id_EmpleadocapacitacionInterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesInternas.objects.get(Id_Empleadocapacitacioninterna=Id_EmpleadocapacitacionInterna)
#         Capacitacion = EmpleadoCapacitacionesInternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadointernounidadesdeapoyo')
                
# # Ingresar Unidades De Apoyo Externo
# class UnidadesDeApoyoExternoView(LoginRequiredMixin,View):
#     def get(self , request):

#         first_name = self.request.user.first_name
#         last_name = self.request.user.last_name
#         NombreCompleto = first_name + ' ' + last_name

#         greeting = {}
#         greeting['heading'] = "Capacitación"
#         greeting['pageview'] = "Externo"
#         greeting['EmpleadoCapacitacionesExternas'] = EmpleadoCapacitacionesExternas.objects.filter(NombreEmpleado=NombreCompleto)

#         return render (request,'capacitaciones/empleadoexternounidadesdeapoyo.html',greeting)

#     def post(self,request):

#         # Crear datos Unidades De Apoyo externo
#         form = EmpleadoCapacitacionesExternasFrom(request.POST)
#         if form.is_valid:
#             form.save()
#             messages.success(request, "Su asistencia se guardo correctamente")
#             return redirect('empleadoexternounidadesdeapoyo')
        
# # Actualizar datos Unidades De Apoyo externo
# def UpdateUnidadesDeApoyoExterno(request,Id_EmpleadocapacitacionExterna):
#     if request.method == 'POST':
#         form = EmpleadoCapacitacionesExternas.objects.get(Id_EmpleadocapacitacionExterna=Id_EmpleadocapacitacionExterna)
#         Capacitacion = EmpleadoCapacitacionesExternasFrom(request.POST, instance=form)
#         if Capacitacion.is_valid:
#             Capacitacion.save()
#             return redirect('empleadoexternounidadesdeapoyo')