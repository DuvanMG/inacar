from django.urls import path
from capacitaciones import views

urlpatterns = [
    #Administrador
    path('creacion-capacitaciones',views.CreacionCapacitacionView.as_view(),name='creacion-capacitaciones'),
    path('capacitaciones-internas-personal',views.InternaPersonalView.as_view(),name='capacitaciones-internas-personal'),
    path('capacitaciones-externas-personal',views.ExternaPersonalView.as_view(),name='capacitaciones-externas-personal'),
    path('UpdateCapacitacion/<str:id>',views.UpdateCapacitacion,name='UpdateCapacitacion'),
    path('UpdatePersonalCapacitacionInterno/<str:id>',views.UpdatePersonalCapacitacionInterno,name='UpdatePersonalCapacitacionInterno'),
    path('UpdatePersonalCapacitacionExterno/<str:id>',views.UpdatePersonalCapacitacionExterno,name='UpdatePersonalCapacitacionExterno'),

    #Gestión Admin
    path('admincapacitacioninterna',views.AdminCapacitacionInternoView.as_view(),name ='admincapacitacioninterna'),
    path('UpdateAdminCapacitacionInterno/<str:id>',views.UpdateAdminCapacitacionInterno,name='UpdateAdminCapacitacionInterno'),
    path('admincapacitacionexterna',views.AdminCapacitacionExternoView.as_view(),name ='admincapacitacionexterna'),
    path('UpdateAdminCapacitacionExterno/<str:id>',views.UpdateAdminCapacitacionExterno,name='UpdateAdminCapacitacionExterno'),

    #Gestión Director
    path('directorcapacitacioninterno',views.DirectorCapacitacionInternoView.as_view(),name ='directorcapacitacioninterno'),
    path('UpdateDirectorCapacitacionInterno/<str:id>',views.UpdateDirectorCapacitacionInterno,name='UpdateDirectorCapacitacionInterno'),
    path('directorcapacitacionexterno',views.DirectorCapacitacionExternoView.as_view(),name ='directorcapacitacionexterno'),
    path('UpdateDirectorCapacitacionExterno/<str:id>',views.UpdateDirectorCapacitacionExterno,name='UpdateDirectorCapacitacionExterno'),

    #Gestión Lider
    path('lidercapacitacioninterno',views.LiderCapacitacionInternoView.as_view(),name ='lidercapacitacioninterno'),
    path('UpdateLiderCapacitacionInterno/<str:id>',views.UpdateLiderCapacitacionInterno,name='UpdateLiderCapacitacionInterno'),
    path('lidercapacitacionexterno',views.LiderCapacitacionExternoView.as_view(),name ='lidercapacitacionexterno'),
    path('UpdateLiderCapacitacionExterno/<str:id>',views.UpdateLiderCapacitacionExterno,name='UpdateLiderCapacitacionExterno'),

    #Gestión Empleado
    path('empleadocapacitacioninterno',views.EmpleadoCapacitacionInternoView.as_view(),name ='empleadocapacitacioninterno'),
    path('UpdateEmpleadoCapacitacionInterno/<str:id>',views.UpdateEmpleadoCapacitacionInterno,name='UpdateEmpleadoCapacitacionInterno'),
    path('empleadocapacitacionexterno',views.EmpleadoCapacitacionExternoView.as_view(),name ='empleadocapacitacionexterno'),
    path('UpdateEmpleadoCapacitacionExterno/<str:id>',views.UpdateEmpleadoCapacitacionExterno,name='UpdateEmpleadoCapacitacionExterno'),

    #Promotora
    # path('empleadointernopromotora',views.PromotoraInternoView.as_view(),name ='empleadointernopromotora'),
    # path('empleadoexternopromotora',views.PromotoraExternoView.as_view(),name ='empleadoexternopromotora'),
    # path('UpdatePromotoraExterno/<str:Id_EmpleadocapacitacionExterna>',views.UpdatePromotoraExterno,name='UpdatePromotoraExterno'),
    # path('UpdatePromotoraInterno/<str:Id_EmpleadocapacitacionInterna>',views.UpdatePromotoraInterno,name='UpdatePromotoraInterno'),

    #Constructora
    # path('empleadointernoconstructora',views.ConstructoraInternoView.as_view(),name ='empleadointernoconstructora'),
    # path('empleadoexternoconstructora',views.ConstructoraExternoView.as_view(),name ='empleadoexternoconstructora'),
    # path('UpdateConstructoraExterno/<str:Id_EmpleadocapacitacionExterna>',views.UpdateConstructoraExterno,name='UpdateConstructoraExterno'),
    # path('UpdateConstructoraInterno/<str:Id_Empleadocapacitacioninterna>',views.UpdateConstructoraInterno,name='UpdateConstructoraInterno'),

    # #Inmobiliaria
    # path('empleadointernoinmobiliaria',views.InmobiliariaInternoView.as_view(),name ='empleadointernoinmobiliaria'),
    # path('empleadoexternoinmobiliaria',views.InmobiliariaExternoView.as_view(),name ='empleadoexternoinmobiliaria'),
    # path('UpdateInmobiliariaExterno/<str:Id_EmpleadocapacitacionExterna>',views.UpdateInmobiliariaExterno,name='UpdateInmobiliariaExterno'),
    # path('UpdateInmobiliariaInterno/<str:Id_EmpleadocapacitacionInterna>',views.UpdateInmobiliariaInterno,name='UpdateInmobiliariaInterno'),

    # #Unidades De Apoyo
    # path('empleadointernounidadesdeapoyo',views.UnidadesDeApoyoInternoView.as_view(),name ='empleadointernounidadesdeapoyo'),
    # path('empleadoexternounidadesdeapoyo',views.UnidadesDeApoyoExternoView.as_view(),name ='empleadoexternounidadesdeapoyo'),
    # path('UpdateUnidadesDeApoyoInterno/<str:Id_EmpleadocapacitacionInterna>',views.UpdateUnidadesDeApoyoInterno,name='UpdateUnidadesDeApoyoInterno'),
    # path('UpdateUnidadesDeApoyoExterno/<str:Id_EmpleadocapacitacionExterna>',views.UpdateUnidadesDeApoyoExterno,name='UpdateUnidadesDeApoyoExterno'),

]