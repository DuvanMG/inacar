import zipfile
import os

# Ruta del archivo comprimido y directorio de destino para la extracción
archivo_comprimido = '/home/DuvanMG/inacar/Admin/indicadores.zip'
directorio_destino = '/home/DuvanMG/inacar/Admin/'

# Crear el directorio de destino si no existe
if not os.path.exists(directorio_destino):
    os.makedirs(directorio_destino)

# Extraer el archivo ZIP
with zipfile.ZipFile(archivo_comprimido, 'r') as zip_ref:
    zip_ref.extractall(directorio_destino)

print('Archivo extraído correctamente.')

# python descomprimir.py

    # if promedio_por_competencia_evaluado:
    #     total_competencias = len(promedio_por_competencia_evaluado)
    #     sum_promedio = sum(item[1] for item in promedio_por_competencia_evaluado)
    #     promedio_total_evaluado = sum_promedio / total_competencias
    #     promedio_total_evaluado_porcentaje = (sum_promedio / total_competencias) * 0.2

    # if promedio_por_competencia_evaluador:
    #     non_none_items = [item[1] for item in promedio_por_competencia_evaluador if item[1] is not None]
    #     if non_none_items:
    #         total_competencias = len(non_none_items)
    #         sum_promedio = sum(non_none_items)
    #         promedio_total_evaluador = sum_promedio / total_competencias
    #         promedio_total_evaluador_porcentaje = (sum_promedio / total_competencias) * 0.8
    #     else:
    #         promedio_total_evaluador = 0
    # else:
    #     promedio_total_evaluador = 0