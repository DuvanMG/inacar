from django.apps import AppConfig
from django.conf import settings
import os

class PagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pages'
    path = os.path.join(settings.BASE_DIR, 'pages')