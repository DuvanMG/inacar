from django.urls import path
from pages import views
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import (PasswordChangeView, PasswordChangeDoneView,PasswordResetView,PasswordResetDoneView, PasswordResetConfirmView,PasswordResetCompleteView)
urlpatterns = [
    path('',views.DashboardView,name='dashboard'),
    # path('lider',views.LiderView,name ='lider'),

    # Authentication
    #Viewscreen 1
    path('account/login/',views.AuthLoginView,name ='authlogin'),
    path('auth-register',views.AuthRegisterView.as_view(),name ='authregister'),
    path('auth-lock-screen',views.AuthLockScreenView.as_view(),name ='authlockscreen'),
    path('auth-authrecoverpw',views.AuthRecoverpwView.as_view(),name ='authrecoverpw'),
    path('auth-change-password',views.AuthChangePasswordView.as_view(),name ='passwordchange'),
    path('auth-confirm-mail',views.AuthConfirmMailView.as_view(),name ='confirmmail'),
    path('auth-email-verificaton',views.AuthEmailVerificationView.as_view(),name ='emailverificaton'),
    path('auth-two-step-verificaton',views.AuthTwoStepVerificationView.as_view(),name ='twostepverification'),
    #Viewscreen 2
    path('auth-login-2',views.AuthLogin2View.as_view(),name ='authlogin2'),
    path('auth-register-2',views.AuthRegister2View.as_view(),name ='authlregister2'),
    path('auth-recoverpw-2',views.AuthRecoverpw2View.as_view(),name ='authrecoverpw2'),
    path('auth-lock-screen-2',views.AuthLockScreen2View.as_view(),name ='authlockscreen2'),
    path('auth-change-password-2',views.AuthChangePassword2View.as_view(),name ='passwordchange2'),
    path('auth-confirm-mail-2',views.AuthConfirmMail2View.as_view(),name ='confirmmail2'),
    path('auth-email-verificaton-2',views.AuthEmailVerification2View.as_view(),name ='emailverificaton2'),
    path('auth-two-step-verificaton-2',views.AuthTwoStepVerification2View.as_view(),name ='twostepverification2'),
]

