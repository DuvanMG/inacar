from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin

class User(AbstractUser, PermissionsMixin):

    is_admin = models.BooleanField('Administrador', default=False)
    is_director = models.BooleanField('Director', default=False)
    is_lider = models.BooleanField('Lider', default=False)
    is_empleado = models.BooleanField('Empleado', default=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name
    
    # def __str__(self):
    #     return f'{self.first_name} {self.last_name}'
    
    @property
    def get_nombre(self):
        return self.get_full_name