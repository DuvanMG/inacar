from django.shortcuts import render, redirect, HttpResponse
from django.views import View
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from .forms import UserRegistrationForm, LoginForm

# Inicio
def DashboardView(request):
    return render (request,'authentication/auth-login.html')
    
class AuthRegisterView(LoginRequiredMixin,View):
    def get(self , request):  
        data = {
            'form':UserRegistrationForm()
        }  
        if request.method == 'POST':
            User_Registration_Form = UserRegistrationForm(data=request.POST)
            if User_Registration_Form.is_valid():
                User_Registration_Form.save()
                user = authenticate(username = User_Registration_Form.cleaned_data['username'], password = User_Registration_Form.cleaned_data['password'])
                login(request, user)
                return redirect('/')
        return render (request,'authentication/auth-register.html',data)

# Iniciar Sesion
def AuthLoginView(request):
    if request.method == 'GET':

        form = LoginForm
        msg = None
        return render (request,'authentication/auth-login.html',{'form':form,'msg':msg})
    else:
        form = LoginForm
        msg = None
        user = authenticate(request, username = request.POST['login'], password = request.POST['password'])

        if user is not None and user.is_admin:
            login(request,user)
            return redirect('creacion-capacitaciones')
        elif user is not None and user.is_director:
            login(request,user)
            return redirect('directorcapacitacioninterno')
        elif user is not None and user.is_lider:
            login(request,user)
            return redirect('lidercapacitacioninterno')
        elif user is not None and user.is_empleado:
            login(request,user)
            return redirect('empleadocapacitacioninterno')
            # if user is not None and user.is_promotora:
            #     login(request,user)
            #     return redirect('empleadointernopromotora')
            # elif user is not None and user.is_constructora:
            #     login(request,user)
            #     return redirect('empleadointernoconstructora')
            # elif user is not None and user.is_inmobiliaria:
            #     login(request,user)
            #     return redirect('empleadointernoinmobiliaria')
            # elif user is not None and user.is_UnidadesDeApoyo:
            #     login(request,user)
            #     return redirect('empleadointernounidadesdeapoyo')
            # else:
            #     msg = 'Credenciales invalidados'
        else:
            msg = 'Credenciales invalidados'

        return render (request,'authentication/auth-login.html',{'form':form,'msg':msg})
    
class AuthRecoverpwView(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-recoverpw.html')
class AuthLockScreenView(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-lock-screen.html')
class AuthChangePasswordView(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-password-change.html')
class AuthConfirmMailView(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-confirm-mail.html')
class AuthEmailVerificationView(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-email-verification.html') 
class AuthTwoStepVerificationView(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-two-step-verification.html')  


#Viewscreen 2
class AuthLogin2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-login-2.html')
class AuthRegister2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-register-2.html')
class AuthRecoverpw2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-recoverpw-2.html')
class AuthLockScreen2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-lock-screen-2.html')
class AuthChangePassword2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-password-change-2.html')
class AuthConfirmMail2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-confirm-mail-2.html')
class AuthEmailVerification2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-email-verification-2.html')   
class AuthTwoStepVerification2View(LoginRequiredMixin,View):
    def get(self , request):    
        return render (request,'authentication/auth-two-step-verification-2.html')  
