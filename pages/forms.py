from crispy_forms.helper import FormHelper
from allauth.account.forms import LoginForm,SignupForm,ChangePasswordForm,ResetPasswordForm,ResetPasswordKeyForm,SetPasswordForm
from django import forms

class UserLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['login'].widget = forms.TextInput(attrs={'class': 'form-control mb-2','placeholder':'Introducir Usuario','id':'username'})
        self.fields['login'].label="Usuario"
        self.fields['password'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-2','placeholder':'Introducir Contraseña','id':'password'})
        self.fields['password'].label="Contraseña"
        self.fields['remember'].widget = forms.CheckboxInput(attrs={'class': 'form-check-input'})
        self.fields['remember'].label="recuérdame"

class UserRegistrationForm(SignupForm):
    first_name = forms.CharField()
    last_name = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['email'].widget = forms.EmailInput(attrs={'class': 'form-control mb-1','placeholder':'Introducir Email','id':'email'})
        self.fields['email'].label="Email"
        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control mb-1','placeholder':'Introducir Usuario','id':'username1'})
        self.fields['username'].label="Usuario"
        self.fields['first_name'].widget = forms.TextInput(attrs={'class': 'form-control mb-1','placeholder':'Introducir Nombres','id':'first_name'})
        self.fields['first_name'].label="Nombres"
        self.fields['last_name'].widget = forms.TextInput(attrs={'class': 'form-control mb-1','placeholder':'Introducir Apellidos','id':'last_name'})
        self.fields['last_name'].label="Apellidos"
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-1','placeholder':'Introducir Contraseña','id':'password1'})
        self.fields['password1'].label="Contraseña"
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-1','placeholder':'Confirmar Contraseña','id':'password2'})
        self.fields['password2'].label="Confirmar Contraseña" 

class PasswordChangeForm(ChangePasswordForm):
      def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['oldpassword'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-2','placeholder':'Introducir Contraseña Actual','id':'password3'})
        self.fields['password2'].label="Contraseña Actual"
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-2','placeholder':'Introducir Nueva Contraseña','id':'password4'})
        self.fields['password2'].label="Nueva Contraseña"
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-2','placeholder':'Introducir Confirmar Contraseña','id':'password5'})
        self.fields['password2'].label="Confirmar Contraseña"

class PasswordResetForm(ResetPasswordForm):
      def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['email'].widget = forms.EmailInput(attrs={'class': 'form-control mb-2','placeholder':'Introducir Email','id':'email1'})
        self.fields['email'].label="Email"

class PasswordResetKeyForm(ResetPasswordKeyForm):
      def __init__(self, *args, **kwargs):
        super(PasswordResetKeyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-2','placeholder':'Enter new password','id':'password6'})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-1','placeholder':'Enter confirm password','id':'password7'})
        self.fields['password2'].label="Confirm Password"

class PasswordSetForm(SetPasswordForm):
      def __init__(self, *args, **kwargs):
        super(PasswordSetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control mb-2','placeholder':'Enter new password','id':'password8'})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control','placeholder':'Enter confirm password','id':'password9'})
        self.fields['password2'].label="Confirm Password"